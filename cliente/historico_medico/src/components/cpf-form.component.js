import React, { Component } from 'react';
import axios from 'axios';
import $ from 'jquery';
import "bootstrap/dist/js/bootstrap.min.js";
import Chart from 'chart.js';

export default class CpfForm extends Component {
    constructor(props) {
        super(props);

        this.onChangeCpf = this.onChangeCpf.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleResponse = this.handleResponse.bind(this);
        this.modalShow = this.modalShow.bind(this);
        this.renderCharts = this.renderCharts.bind(this);
        this.renderChart = this.renderChart.bind(this);
        this.showMessage = this.showMessage.bind(this);
        this.showBio = this.showBio.bind(this);

        this.state = {
            cpf: '',
            cpfForm: $('#cpfForm'),
            backButton: $('#backButton')
        }
    }

    componentDidMount() {
        this.setState({
            cpfForm: $('#cpfForm'),
            backButton: $('#backButton')
        });

        this.state.backButton.hide();
        $('.chart').hide();
        $('.info').hide();

        this.state.backButton.click(() => {
            this.state.cpfForm.show();
            this.state.backButton.hide();
            $('.chart').hide();
            $('.info').hide();
            $('.info span').text('');
            $('.message h3').text('');

            $('.chartCanvas').each(function(){
                var canvas = $(this).get(0);
                var context = canvas.getContext('2d');
                context.clearRect(0, 0, canvas.width, canvas.height);
            });

            Chart.helpers.each(Chart.instances, function(instance){
                instance.destroy()
            })
        });
    }

    onChangeCpf(e) {
        this.setState({
            cpf: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        axios.get('http://127.0.0.1:5003/isalive/')
            .then((this.handleResponse))
            .catch((error) => {
                if (error.message.includes('Network Error')) {
                    this.modalShow("Desculpe, serviço fora do ar.");
                } else {
                    this.modalShow("Desculpe, erro ao acessar serviço.");
                }
              });
    }

    handleResponse(res) {
        if (res.data !== 'yes') {
            this.modalShow('Desculpe, sistema em manutenção.');

            return;
        }

        axios.get('http://127.0.0.1:5003/dados/'+this.state.cpf)
        .then((res) => {
            if (res.data.erro) {
                this.modalShow(res.data.erro);
            } else {
                this.state.cpfForm.hide();
                this.state.backButton.show();
                this.showBio(res.data);

                this.renderCharts();
            }          
        })
        .catch((error) => {
            this.modalShow("Desculpe, erro ao acessar serviço.");
        });
    }

    modalShow(message) {
        var messageModal = $('#messageModal');
        messageModal.find('.modal-body').text(message);
        messageModal.modal();
    }

    showMessage(chart, message) {
        $('#'+chart+'Message h3').text(message);
        $('.'+chart).show();
    }

    showBio(data) {
        console.log(data);
        $('.nome').text(data.nome);
        $('.idade').text(data.idade);
        $('.sexo').text(data.sexo == 'M' ? 'Masculino' : 'Feminino');
        $('.cpf').text(data.cpf);
        $('.foto').attr('src', '../assets/img/'+data.foto+'.jpg');

        $('.info').show();
    }

    renderCharts() {
        // glicose
        var renderGlicoseChart = () => {
            axios.get('http://127.0.0.1:5000/dados/'+this.state.cpf)
            .then((res) => {
                console.log(res.data);

                var labels = [];
                var values = [];

                for (let i = 0; i < res.data.length; i++) {
                    var date = res.data[i].data.split('-');
                    labels.push(date[2]+'/'+date[1]+'/'+date[0]);
                    values.push(res.data[i].valor);
                }

                var chartData = {
                    labels: labels,
                    datasets: [{
                        label: "mg/dL",
                        data: values,
                        "fill": false,
                        "borderColor": "rgb(112, 205, 205)",
                        "lineTension": 0.1
                    }]
                };

                this.renderChart('glicose', chartData, 'Taxa de glicose', 0, 400);
            })
            .catch((error) => {
                this.showMessage('glicose', 'Desculpe, erro ao acessar serviço de glicose.');
            });
        };
        
        axios.get('http://127.0.0.1:5000/isalive/')
        .then((res) => {
            if (res.data !== 'yes') {
                this.showMessage('glicose', 'Desculpe, serviço de glicose em manutenção.');
            } else {
                renderGlicoseChart();
            }
        })
        .catch((error) => {
            this.showMessage('glicose', 'Desculpe, erro ao acessar serviço de glicose.');
        });


        // oximetria
        var renderOximetriaChart = () => {
            axios.get('http://127.0.0.1:5001/dados/'+this.state.cpf)
            .then((res) => {
                console.log(res.data);

                var labels = [];
                var values = [];

                for (let i = 0; i < res.data.length; i++) {
                    var date = res.data[i].data.split('-');
                    labels.push(date[2]+'/'+date[1]+'/'+date[0]);
                    values.push(res.data[i].valor);
                }

                var chartData = {
                    labels: labels,
                    datasets: [{
                        label: "porcentagem",
                        data: values,
                        "fill": false,
                        "borderColor": "rgb(174, 134, 255)",
                        "lineTension": 0.1
                    }]
                };

                this.renderChart('oximetria', chartData, 'Saturação de oxigênio no sangue', 0, 100);
            })
            .catch((error) => {
                this.showMessage('oximetria', 'Desculpe, erro ao acessar serviço de oximetria.');
            });
        };

        axios.get('http://127.0.0.1:5001/isalive/')
        .then((res) => {
            if (res.data !== 'yes') {
                this.showMessage('oximetria', 'Desculpe, serviço de oximetria em manutenção.');
            } else {
                renderOximetriaChart();
            }
        })
        .catch((error) => {
            this.showMessage('oximetria', 'Desculpe, erro ao acessar serviço de oximetria.');
        });


        //pressao arterial

        var renderPressaoArterialChart = () => {
            axios.get('http://127.0.0.1:5002/dados/'+this.state.cpf)
            .then((res) => {
                console.log(res.data);

                var labels = [];
                var diastolica = [];
                var sistolica = [];

                for (let i = 0; i < res.data.length; i++) {
                    var date = res.data[i].data.split('-');
                    labels.push(date[2]+'/'+date[1]+'/'+date[0]);
                    diastolica.push(res.data[i].valor.diastolica);
                    sistolica.push(res.data[i].valor.sistolica);
                }

                var chartData = {
                    labels: labels,
                    datasets: [
                        {
                            label: "mmHg (diastólica)",
                            data: diastolica,
                            "fill": false,
                            "borderColor": "rgb(54, 162, 235)",
                            "lineTension": 0.1
                        },
                        {
                            label: "mmHg (sistólica)",
                            data: sistolica,
                            "fill": false,
                            "borderColor": "rgb(255, 142, 166)",
                            "lineTension": 0.1
                        }
                    ]
                };

                this.renderChart('pressaoArterial', chartData, 'Pressão arterial', 0, 200);
            })
            .catch((error) => {
                this.showMessage('pressaoArterial', 'Desculpe, erro ao acessar serviço de pressão arterial.');
            });
        };

        axios.get('http://127.0.0.1:5002/isalive/')
        .then((res) => {
            if (res.data !== 'yes') {
                this.showMessage('pressaoArterial', 'Desculpe, serviço de pressão arterial em manutenção.');
            } else {
                renderPressaoArterialChart();
            }
        })
        .catch((error) => {
            this.showMessage('pressaoArterial', 'Desculpe, erro ao acessar serviço de pressão arterial.');
        });

    }

    renderChart(chart, data, title, min, max) {
        var glicoseCtx = $('#'+chart+'Chart');

        var myLineChart = new Chart(glicoseCtx, {
            type: 'line',
            data: data,
            options:{
                title: {
                    display: true,
                    text: title,
                    fontSize: 26
                },
                scales: {
                    yAxes : [{
                        ticks : {
                            max : max,    
                            min : min
                        }
                    }]
                }
            }
        });

        $('.'+chart).show();
    }

    render() {
        return (
            <div id="cpfForm" className="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
                <h3>Histórico Médico</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Informe o CPF do paciente: </label>
                        <input type="text" required className="form-control" value={this.state.cpf} onChange={this.onChangeCpf}/>
                    </div>
                    <div className="form-group">
                        <input type="submit" required className="btn btn-primary" value="Buscar"/>
                    </div>
                </form>
            </div>
        )
    }
}