import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/navbar.component"
import CpfForm from "./components/cpf-form.component"

function App() {
  return (
  <Router>
    <Navbar />
    <br/>
    <Route path="/" exact component={CpfForm} />
  </Router>
  );
}

export default App;
