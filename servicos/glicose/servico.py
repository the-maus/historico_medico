from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
from pymemcache.client import base
import json

servico = Flask(__name__)
CORS(servico, support_credentials=True)

BANCO_VOLATIL = "banco_volatil"

IS_ALIVE = "yes"
VERSION = "0.0.1"
DESCRIPTION = "Servico com dados sobre glicose de pacientes"
AUTHOR = "Matheus Sampaio Rodrigues Santos"
EMAIL = "msampaio.mail@gmail.com"

@servico.route("/isalive/")
def is_alive():
    return IS_ALIVE

@servico.route("/info/")
def get_info():
    info = jsonify(
        version = VERSION,
        description = DESCRIPTION,
        author = AUTHOR,
        email = EMAIL
    )

    return info

@servico.route("/gravar/", methods=["POST", "GET"])
def gravar():
    dados = request.get_json()
    if dados:
        client = base.Client((BANCO_VOLATIL, 11211))
        client.set("glicose", dados)

    return "Ok"

@servico.route("/dados/<cpf>")
def get_dados(cpf):
    resultado = json.loads("{\"erro\":\"dados não inicializados\"}")

    client = base.Client((BANCO_VOLATIL, 11211))
    dados = client.get("glicose")
    if dados:
        resultado = json.loads(dados)
        resultado = resultado[cpf] if (cpf in resultado) else json.loads("{\"erro\":\"Paciente não encontrado\"}")

    return jsonify(resultado)

if __name__ == "__main__":
    servico.run(
        host = "0.0.0.0",
        debug=True
    )