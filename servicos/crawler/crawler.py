import requests
import json
from time import sleep
import os.path

#cpfs gerados aleatoreamente, datas e valores reais, fonte: https://archive.ics.uci.edu/ml/datasets/diabetes
DADOS_GLICOSE = os.path.join(os.path.abspath(os.path.dirname(__file__)), "dados/glicose.json") 

#cpfs gerados aleatoreamente, datas ficcionais e valores reais (um dos valores foi alterado para fins de teste), fonte: https://www.physionet.org/content/osv/1.0.0/
DADOS_OXIMETRIA = os.path.join(os.path.abspath(os.path.dirname(__file__)), "dados/oximetria.json") 

#cpfs gerados aleatoreamente, datas ficcionais e valores reais, fonte: 
# https://ncdrisc.org/data-downloads-blood-pressure.html
# https://data.mendeley.com/datasets/56d8t4xtp5/1
DADOS_PRESSAO_ARTERIAL = os.path.join(os.path.abspath(os.path.dirname(__file__)), "dados/pressao_arterial.json")

#cpfs gerados aleatoreamente, dados ficcionais
DADOS_PACIENTES = os.path.join(os.path.abspath(os.path.dirname(__file__)), "dados/pacientes.json") 

URL_GLICOSE = "http://glicose:5000/gravar/"
URL_OXIMETRIA = "http://oximetria:5000/gravar/"
URL_PRESSAO_ARTERIAL = "http://pressaoarterial:5000/gravar/"
URL_PACIENTES = "http://pacientes:5000/gravar/"

def enviar(url, json_dados):
    arquivo = open(json_dados, "r")
    dados = json.load(arquivo)
    arquivo.close()

    resposta = "nenhum dado para enviar"
    if dados:
        resposta = requests.post(url, json=json.dumps(dados))
        if resposta.ok:
            resposta = "dados enviados"
        else:
            resposta = "erro de envio: " + resposta.text

    return resposta

if __name__ == "__main__":
    while True:
        try:
            resposta = enviar(URL_PACIENTES, DADOS_PACIENTES)
            print(resposta, "[PACIENTES]")

            resposta = enviar(URL_GLICOSE, DADOS_GLICOSE)
            print(resposta, "[GLICOSE]")

            resposta = enviar(URL_OXIMETRIA, DADOS_OXIMETRIA)
            print(resposta, "[OXIMETRIA]")

            resposta = enviar(URL_PRESSAO_ARTERIAL, DADOS_PRESSAO_ARTERIAL)
            print(resposta, "[PRESSAO ARTERIAL]")
        except: 
            print("aguardando execução dos serviços")

        # resposta = enviar(URL_SISTEMAS, NOTICIAS_SISTEMAS)
        # print(resposta, "[SISTEMAS]")

        sleep(10)
