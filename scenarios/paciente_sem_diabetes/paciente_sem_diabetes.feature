Feature: Verificar paciente sem diabetes

  Scenario: Verificação de paciente sem diabetes
     Given o microsserviço de dados sobre glicose está ativo
      When verificar se ocorreu mais de um valor de glicose superior a 125 no histórico de um paciente
      Then certificar que a verificação retornou um valor negativo