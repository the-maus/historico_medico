Feature: Verificar paciente com diabetes

  Scenario: Verificação de paciente com diabetes
     Given o microsserviço de dados sobre glicose está ativo
      When verificar se ocorreu mais de um valor de glicose superior a 125 no histórico de um paciente
      Then certificar que a verificação retornou um valor positivo