from behave import *
from pathlib import Path
import urllib.request, os, subprocess, time, json
import logging

url = "http://127.0.0.1:5000"

def acessar(url):
    response = urllib.request.urlopen(url)
    data = response.read()

    return data.decode("utf-8")

@given('o microsserviço de dados sobre glicose está ativo')
def step_impl(context):
    assert acessar(url+'/isalive/') == "yes"

#fonte: https://cerpe.com.br/saude/glicose-alta-baixa-normal
@when('verificar se ocorreu mais de um valor de glicose superior a 125 no histórico de um paciente')
def step_impl(context):
    response = acessar(url+'/dados/26163575008')
    result = json.loads(response)

    glicose_alta = 0

    for record in result:
        if int(record['valor']) > 125:
            glicose_alta += 1
    
    context.tem_diabetes = glicose_alta > 1
    assert True

@then('certificar que a verificação retornou um valor positivo')
def step_impl(context):
    assert context.tem_diabetes == True
