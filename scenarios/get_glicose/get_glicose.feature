Feature: Obter dados sobre glicose de um paciente

  Scenario: Obtenção de histórico de glicose de um paciente 
     Given o microsserviço de dados sobre glicose está ativo
      When solicitar histórico de glicose de um paciente
      Then verificar se as informações vieram sem erro