Feature: Obter dados sobre oximetria de um paciente

  Scenario: Obtenção de histórico de oximetria de um paciente 
     Given o microsserviço de dados sobre oximetria está ativo
      When solicitar histórico de oximetria de um paciente
      Then verificar se as informações vieram sem erro