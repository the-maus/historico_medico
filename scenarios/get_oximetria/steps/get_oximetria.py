from behave import *
from pathlib import Path
import urllib.request, os, subprocess, time, json

url = "http://127.0.0.1:5001"

def acessar(url):
    response = urllib.request.urlopen(url)
    data = response.read()

    return data.decode("utf-8")

@given('o microsserviço de dados sobre oximetria está ativo')
def step_impl(context):
    assert acessar(url+'/isalive/') == "yes"

@when('solicitar histórico de oximetria de um paciente')
def step_impl(context):
    response = acessar(url+'/dados/26163575008')
    context.data = json.loads(response)
    assert True

@then('verificar se as informações vieram sem erro')
def step_impl(context):
    data = context.data
    assert ('data' in data[0]) and ('valor' in data[0])
