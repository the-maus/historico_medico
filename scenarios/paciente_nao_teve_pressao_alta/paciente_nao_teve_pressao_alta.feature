Feature: Verificar paciente que não teve valores indicativos de pressão alta

  Scenario: Verificação de paciente que não teve valores indicativos de pressão alta
     Given o microsserviço de dados sobre pressão alta está ativo
      When verificar se algum registro do historico de medicoes de pressao de um paciente apresentou valores iguais ou superiores a 14/9
      Then certificar que a verificação retornou um valor negativo