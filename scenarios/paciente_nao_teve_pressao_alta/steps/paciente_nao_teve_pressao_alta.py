from behave import *
from pathlib import Path
import urllib.request, os, subprocess, time, json
import logging

url = "http://127.0.0.1:5002"

def acessar(url):
    response = urllib.request.urlopen(url)
    data = response.read()

    return data.decode("utf-8")

@given('o microsserviço de dados sobre pressão alta está ativo')
def step_impl(context):
    assert acessar(url+'/isalive/') == "yes"

#fonte: https://drauziovarella.uol.com.br/entrevistas-2/complicacoes-da-hipertensao-entrevista/
@when('verificar se algum registro do historico de medicoes de pressao de um paciente apresentou valores iguais ou superiores a 14/9')
def step_impl(context):
    response = acessar(url+'/dados/26163575008')
    result = json.loads(response)

    context.teve_pressao_alta = False

    for record in result:
        if (float(record['valor']['sistolica']) >= 140 and
            float(record['valor']['diastolica']) >= 90):
            context.teve_pressao_alta = True
            break
    
    assert True

@then('certificar que a verificação retornou um valor negativo')
def step_impl(context):
    assert context.teve_pressao_alta == False
