from behave import *
from pathlib import Path
import urllib.request, os, subprocess, time

urls = [
    "http://127.0.0.1:5000", #glicose
    "http://127.0.0.1:5001", #oximetria
    "http://127.0.0.1:5002", #pressao_arterial
    "http://127.0.0.1:5003", #pacientes
]

def acessar(url):
    response = urllib.request.urlopen(url)
    data = response.read()

    return data.decode("utf-8")

@given('todos os microsserviços parados')
def step_impl(context):
    access_error_count = 0
    
    for url in urls:
        try:
            status_code = urllib.request.urlopen(url+'/isalive/').getcode()
            if (status_code != 200):
                access_error_count+=1
        except: #connection error
            access_error_count+=1

    assert access_error_count == len(urls)

@when('rodar o comando para inicializar')
def step_impl(context):
    path = Path(__file__).parents[2]
    os.chdir(path)
    subprocess.Popen(['docker-compose', 'up'], shell=False)
    assert True

@then('testar se os microsserviços estão ativos')
def step_impl(context):
    time.sleep(10) # dando um tempo pros servicos iniciarem

    access_count = 0

    for url in urls:
        if acessar(url+'/isalive/') == "yes":
            access_count+=1

    assert access_count == len(urls)
