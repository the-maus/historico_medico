Feature: Verificar paciente que teve valores indicativos de pressao alta

  Scenario: Verificação de paciente que teve valores indicativos de pressao alta
     Given o microsserviço de dados sobre pressao alta está ativo
      When verificar se algum registro do historico de medicoes de pressao de um paciente apresentou valores iguais ou superiores a 14/9
      Then certificar que a verificação retornou um valor positivo