from behave import *
from pathlib import Path
import urllib.request, os, subprocess, time, json
import logging

url = "http://127.0.0.1:5001"

def acessar(url):
    response = urllib.request.urlopen(url)
    data = response.read()

    return data.decode("utf-8")

@given('o microsserviço de dados sobre oximetria está ativo')
def step_impl(context):
    assert acessar(url+'/isalive/') == "yes"

#fonte: https://www.uol.com.br/vivabem/colunas/paola-machado/2021/03/02/hipoxemia-baixos-niveis-de-oxigenio-no-corpo.htm
@when('verificar se algum registro do historico de medicoes de oximetria de um paciente apresentou valor inferior a 95')
def step_impl(context):
    response = acessar(url+'/dados/18750365045')
    result = json.loads(response)

    context.teve_baixa_saturacao = False

    for record in result:
        if (float(record['valor']) < 95):
            context.teve_baixa_saturacao = True
            break
    
    assert True

@then('certificar que a verificação retornou um valor negativo')
def step_impl(context):
    assert context.teve_baixa_saturacao == False
