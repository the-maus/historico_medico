Feature: Verificar paciente que teve baixa saturacao de oxigenio no sangue

  Scenario: Verificação de paciente com baixa saturacao de oxigenio no sangue
     Given o microsserviço de dados sobre oximetria está ativo
      When verificar se algum registro do historico de medicoes de oximetria de um paciente apresentou valor inferior a 95
      Then certificar que a verificação retornou um valor positivo