Feature: Obter dados sobre pressão arterial de um paciente

  Scenario: Obtenção de histórico de pressão arterial de um paciente 
     Given o microsserviço de dados sobre pressão arterial está ativo
      When solicitar histórico de pressão arterial de um paciente
      Then verificar se as informações vieram sem erro